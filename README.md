# thinkst-console-v0.1t

```
   ______                                ______                       __
  / ____/___ _____  ____ ________  __   / ____/___  ____  _________  / /__
 / /   / __ `/ __ \/ __ `/ ___/ / / /  / /   / __ \/ __ \/ ___/ __ \/ / _ \
/ /___/ /_/ / / / / /_/ / /  / /_/ /  / /___/ /_/ / / / (__  ) /_/ / /  __/
\____/\__,_/_/ /_/\__,_/_/   \__, /   \____/\____/_/ /_/____/\____/_/\___/
                            /____/
```

This project is a technical evaluation developed by [Thabang Chukura](mailto:atchukura@gmail.com) for [Thinkst Applied Research](https://thinkst.com/).

This is a [Vue.js](https://vuejs.org) v3 project.

## 1. Features

The Canary Console is a dashboard application consisting of a statistics overview page, as well as sections for users to view Incidents and their Canaries. The following features have been implemented:

- Dashboard - Stats, charts, graphs, computed properties
- Incidents - DataTable
- Canaries - Routing to singleton Canary pages
- Components - Canaries, Charts exist as card components leveraging props
- Store (Vuex) for faster UI rendering, by avoiding repeated API calls

The project has been developed with a "minimum shippable product" approach, meaning unnecessary UI elements have been ommited in favour of strictly fully functional UI elements. If you can see it, it does something. All design nice-to-haves and flourishes are considered future improvements. Refer to Future Improvements below in Section 5.

### 1.1 Project Tooling, Packages

- Linting with ESLint + Prettier
- [Tailwind CSS](https://tailwindcss.com/)
- [Apex Charts](https://apexcharts.com/)

## 2. Project setup

```
npm install
```

### 2.1 Compiles and hot-reloads for development

```
npm run serve
```

### 2.2 Compiles and minifies for production

```
npm run build
```

### 2.3 Run your unit tests

```
npm run test:unit
```

### 2.4 Lints and fixes files

```
npm run lint
```

### 2.5 Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## 3. CI/CD

Continuous integration and deployment leverages Bitbucket Pipelines, and configuration can be edited in the `bitbucket-pipelines.yml` file.

Pushing to `master` will deploy to an AWS s3 bucket configured for static site hosting.

## 4. Live Site

You can visit the live deployed site by visiting [https://thinkst-console.s3.us-east-2.amazonaws.com/](https://thinkst-console.s3.us-east-2.amazonaws.com/).

## 5. Future Improvements

The following items will be addressed in a future release:

- More useful statistics
- General refactoring of `populateData()` method for possible performance improvements, async await
- UI/UX solution for if API requests return server errors; toasts, unhappy page, cache reliance
- Node/Express middleware to provide unique methods/endpoints for `alerts` and `device_list` requests, caching requests
- SQL calculated fields, or MongoDB aggregations for more performant stats requests
- More robust unit test coverage
- TypeScript for stronger typing
- More robust browser testing
- General UI/UX improvements
- DataTable search
- Incident notifications; live database or similar, with data/interface refresh on notification interaction
- Auth pages, and route guarding
