import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "./index.css";
import VueApexCharts from "vue3-apexcharts";
import PrimeVue from "primevue/config";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import InputText from "primevue/inputtext";
import Button from "primevue/button";
import "primevue/resources/themes/md-light-indigo/theme.css";
import "primeicons/primeicons.css";

const app = createApp(App);
app.use(store);
app.use(router);
app.use(VueApexCharts);
app.use(PrimeVue);
app.mount("#app");

app.component("DataTable", DataTable);
app.component("Column", Column);
app.component("InputText", InputText);
app.component("Button", Button);
