import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Incidents from "../views/Incidents.vue";
import Canaries from "../views/Canaries.vue";
import Canary from "../views/Canary.vue";
import PageNotFound from "../views/404.vue";

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/incidents",
    name: "Incidents",
    component: Incidents,
  },
  {
    path: "/canaries",
    name: "Canaries",
    component: Canaries,
  },
  {
    path: "/canaries/:id",
    name: "Canary",
    component: Canary,
  },
  {
    path: "/:pathMatch(.*)*",
    name: "PageNotFound",
    component: PageNotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
