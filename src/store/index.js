import { createStore } from "vuex";

export default createStore({
  state: {
    alerts: [],
    deviceList: [],
  },
  getters: {
    getAlerts: (state) => {
      return state.alerts;
    },
    getDeviceList: (state) => {
      return state.deviceList;
    },
  },
  mutations: {
    setAlerts: (state, data) => {
      state.alerts = data;
    },
    setDeviceList: (state, data) => {
      state.deviceList = data;
    },
  },
  actions: {
    setAlerts: (context) => {
      context.commit("setAlerts");
    },
    setDeviceList: (context) => {
      context.commit("setDeviceList");
    },
  },
  modules: {},
});
