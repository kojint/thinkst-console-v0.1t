import axios from "axios";
import { populateData, API } from "./populateData";

jest.mock("axios");

describe("populateData", () => {
  it("fetches successful data from an API", async () => {
    const data = {
      data: {
        alerts: [
          {
            created: "1438253021",
            key: "incident:httplogin:172.15.187.125:1438253021",
            node_id: "0000000007a9e025",
          },
          {
            created: "1438607783",
            key: "incident:devicedied::1438607783",
            node_id: "0000000007a9e025",
          },
        ],
      },
    };

    axios.get.mockImplementationOnce(() => Promise.resolve(data));
    await expect(populateData()).resolves.toEqual(data);

    expect(axios.get).toHaveBeenCalledWith(API);
  });
  it("catches erroneous responses from an API", async () => {
    const error = "Network Error";

    axios.get.mockImplementationOnce(() => Promise.reject(new Error(error)));
    await expect(populateData()).rejects.toThrow(error);
  });
});
