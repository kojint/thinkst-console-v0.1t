import axios from "axios";

export const API = process.env.VUE_APP_API;
export const populateData = async () => {
  const url = API;

  return await axios.get(url);
};

populateData();
